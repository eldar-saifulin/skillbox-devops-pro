#!/bin/bash

if [ -d ./html ] ; then

	# 1. Проверка папки бекапирования
	if [ -d /var/www/backup ] ; then
		echo "Папка /var/www/backup существует"
	else
		echo "Папка /var/www/backup НЕ существует"
		if mkdir /var/www/backup ; then
			echo "Папка создана!"
		else
			echo "Ошибка при создании папки /var/www/backup"
			exit 1
		fi
	fi

	# 2. Архивирование
	backup_filename="/var/www/backup/html-$(date +"%d%m%y-%H%M%S").tar.gz"
	if tar -czf "$backup_filename" -C /var/www/ html ; then
		echo "Архив создан: $backup_filename"
	else
		echo "Ошибка при создании архива"
		exit 1
	fi

	# 3. Очистка папки перед копированием
	if rm -rf /var/www/html/* ; then
		echo "Папка /var/www/html очищена перед копированием"
	else
		echo "Ошибка при очистке папки /var/www/html"
		exit 1
	fi

	# 4. Копирование сайта
	if cp -r ./html/* /var/www/html ; then
		echo "Сайт скопирован"
	else
		echo "Ошибка при копировании сайта"
		exit 1
	fi

else
	echo "ОШИБКА: Папка ./html не существует!"
	echo "Проверьте наличие папки в репозитории"
	exit 1
fi

